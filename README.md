# utils
`utils` is a pattern for managing bash utils that enforces self-documentation, adds completion, and provides a framework for keeping your scripts organized.

## installation
The `install` script will source utils.bash in your `~/.bashrc`.

```bash
$ ./install
```

## usage
After installing `utils`, run the following at the root of project for which you want to manage utils:

```bash
$ utils init
```

Then run:

```bash
$ utils new test
```

This generates a new util with example code that follows the pattern for you to follow. Now try the following:

```bash
$ utils test
$ utils test run
```

Notice that you have tab completion, then run:

```bash
$ utils test now
$ utils test later
```

## pattern
When `utils` is run, it looks for a `util` folder in the current directory and either prints the documentation or runs a util script if a script name is given. If a given script exists, it will either run it, or print its documentation.

### docs
Each script file must have a `usage(){ ... }` that prints out docs for the script. The script should run this if given no arguments. The first line of the usage should have a brief description, which is used for top-level docs.

### completion
Each script may optionally also have a `completion(){ ... }` function that should print out all options for completion. It is passed the previous and current command strings. For example:

```bash
completion() {
  prev=$1
  cur=$2
  if [ "$prev" = "run" ]; then
    echo now later
  fi
}
```

This would provide tab comletion options `now` and `later` when you have already typed `utils <command> run` and press tab.

## helpers
`utils` comes with two helpers out of the box: `init` and `new`. `init` creates a `util` folder structure. `new` creates a new util in your `util` folder with the correct structure and example command.


## scripting in not bash
A `util/` folder should have a `src/` subfolder for holding source files for other languages. There must still be a bash script or binary in the `util/` folder that runs the source.

## docker
Scripts can be run inside a running docker container if utils is passed a container name or substring to match against to find the container.

## options and flags
`utils` does not provide any framework (therefore no restrictions) on how you handle flags and options. It only finds the appropriate script and passes it everything in the command string after the script name.
