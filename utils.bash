# utils is a source-able script to give default options to all of our util scripts,
# as well as completion and enforcement of good patterns in writing them.

# does some crazy crap to generate columnized auto docs for top level commands
usage() {
	# print global options
cat<<HERE
Usage: utils OPTIONS <command> OPTIONS ARGUMENTS

Options:
HERE

cat > /tmp/utils-usage.txt <<HERE
  -g(global)  <container>%@  use global utils
  %@
  -c(container)  <container>%@  specify container name 
  %@
  -s(substring)  <substring>%@  search for a container containing <substring> and run command on that
  %@
  -h(help)%@  show help
  %@
  -l%@  don't redirect logs to standard out
  %@
HERE
cat /tmp/utils-usage.txt | column -t -c 80 -s '%@'

echo Commands:

cat > /tmp/utils-usage.txt <<HERE
  init%@  Initializes a folder with a util folder and its subfolders.
  %@
  new <name>%@  Creates a new util
  %@
HERE

  # add command names
  for f in `ls $wd/util | grep -vE 'bin|src|.bash'`; do
    desc=`source $wd/util/$f > /dev/null; usage | head -1`
    echo "  $f%@$desc" >> /tmp/utils-usage.txt
    echo " %@ " >> /tmp/utils-usage.txt
  done
  cat /tmp/utils-usage.txt | column -t -c 80 -s '%@'
  rm /tmp/utils-usage.txt
}

initutils() {
  if [ -d util ]; then
    echo Folder named '"util"' already exists.
    return 1
  fi

  echo Creating util folder structure...
  mkdir util
  mkdir util/src
  sleep 1
  echo Done.
}

newtil() {
  cmd=$1
  if [ -z "$cmd" ]; then
    echo Please provide a name for your new util.
    return
  fi
  if [ ! -d $wd/util ]; then
    echo No util folder found, please init.
  fi
  echo creating $cmd util...
  cat > $wd/util/$cmd <<HEREDOC
#!/bin/bash

usage() {
  cat<<HERE
  a util for doing a cool thing

  Usage: <command> [args...]

  Commands:
    run   Runs the $cmd script
HERE
}

completion() {
  prev=\$1
  cur=\$2
  if [ "\$prev" = "run" ]; then
    echo now later
  fi
}

$cmd-run() {
  echo doing the thing...
  echo the rest of your args are:
  echo \$@
}

cmd=\$1
args=\${@:2}

# print usage if no command is passed
if [ "\$cmd" = "run" ]; then
  $cmd-run \$args
else
  usage
fi
HEREDOC

  chmod +x util/$cmd
  echo done
}

utils () {
  local OPTIND opt container help
  local log=true
  local container=none
  wd=$(pwd)

  # handle opts
  while getopts "ghc:s:l" opt; do
    case $opt in
      g) global=true ;;
      c) container=$OPTARG ;;
      s) substring=$OPTARG ;;
      h) help=true ;;
      l) log=false ;;
    esac
  done

  if [ "$global" = true ]; then
    wd=$HOME
  fi

  # init
  if [ "$1" = "init" ]; then
    initutils
    return

  # new
  elif [ "$1" = "new" ]; then
    if [ -z "$2" ]; then
      echo please provide a name
    else
      newtil $2
    fi
    return
  fi

  # check util path if not dockering
  if [ ! -d "$wd/util/src" ] && [ ! -d "$wd/util" ] && [ -z $substring ] && [ "$container" = "none" ]; then
    echo 'Could not find "util" folder. Are you in the right folder?'
    return 1
  fi

  # find container if not set to "none"
  if [ -z "$container" ]; then
    if [ ! -z "$substring" ]; then
      container=`docker ps | grep "$substring" | awk '{print $(NF)}'`
    else
      container=`docker ps | grep "_${PWD##*/}$" | awk '{print $(NF)}'`
    fi
  fi

  cmd=${@:$OPTIND:1}
  args="${@:$OPTIND+1}"

  # print help
  if [ $help ] || [ -z "$cmd" ] || [ -z "${@:$OPTIND+1:1}" ]; then
    # run command with no args to get usage if we have it
    if [ ! -z "$cmd" ]; then
      $wd/util/$cmd
    else
      usage
    fi

  # just pass along the remaining args
  elif [ ! -z $cmd ] && [ !$help ]; then
    # no container, just run util
    if [ "$container" = "none" ]; then
      $wd/util/$cmd ${args[@]}

    # could not find container
    elif [ -z "$container" ]; then
      echo Could not find container ending in _${PWD##*/}.
      echo If you meant to run locally, pass "-c none"

    # run in container
    else
      if [ "$log" == true ]; then
        docker exec $container bash -c "$wd/util/$cmd ${args[@]}"
      else
        docker exec $container bash -c "$wd/util/$cmd ${args[@]} >/proc/1/fd/1 2>&1"
      fi
    fi
  fi
}

_utils () {
  cur="${COMP_WORDS[COMP_CWORD]}"
  prev="${COMP_WORDS[COMP_CWORD-1]}"
  cmd="${COMP_WORDS[1]}"
  wd=$(pwd)

  # if we're globalling
	if [ "${COMP_WORDS[1]}" = "-g" ]; then
    wd=$HOME
		cmd="${COMP_WORDS[2]}"
	fi


  if [ ! -d $wd/util ]; then
    return
  fi

  # set prevprev if array is long enough
  # for setting container
  if [ ${#COMP_WORDS[@]} -gt 2 ]; then
    prevprev="${COMP_WORDS[COMP_CWORD-2]}"
  fi

  # if we have flags
	if [ ! -z "$(echo $cmd | grep -E '\^-[A-Za-z]*')" ]; then
		cmd="${COMP_WORDS[2]}"
	fi

  # if we have the flag the takes an arg
	if [ "${COMP_WORDS[1]}" = "-c" ]; then
		cmd="${COMP_WORDS[3]}"
	fi

  # if we passed a container
  if [ "$prevprev" = "-c" ]; then
    cmd=
  fi

  # if we're not done typing the command
	if [ "$cur" = "$cmd" ]; then
		cmd=
	fi

  # first word
  if [ -z "$cmd" ]; then
		if [ ! -z "$(echo $cur | grep -E '^\-[A-Za-z]*')" ]; then
      words="-h -c -g"
    elif [ "$prev" = "-c" ]; then
      words="none"
		else
      words=$(ls $wd/util | grep -vE 'bin|src|utils')
      words="${words[@]} init new"
		fi
  elif [ -z "$(echo $cmd | grep -E 'new|init')" ]; then
    words=$(get_completion "$cmd" "$prev" "$cur")
  fi

  COMPREPLY=( $(compgen -W "${words}" -- ${cur}) )
}

get_completion () {
  cmd=$1
  prev=$2
  cur=$3
  commands=`$wd/util/$cmd | sed '1d' | grep -vE '^ *[A-Za-z\-]*: *|^$|^      ' | sed 's/ *\([A-Za-z\-]*\).*/\1/'`

  # options
  if [ ! -z "$(echo $cur | grep -E '\-[A-Za-z]*')" ]; then
    $wd/util/$cmd | grep -E '^ *-' | sed 's/ *\(-[A-Za-z]*\).*/\1/'

  # commands (not indented beyond 6 spaces)
  elif [ "$prev" = "$cmd" ] && [ ! -z "${commands[@]}" ]; then
    echo "${commands[@]}"

  # custom completion
  else
    current_args=( "$@" )
    set -- ""
    source $wd/util/$cmd > /dev/null
    set -- "${current_args[@]}"
    completion_exists=$(type completion 2>&1)
    if [ ! -z "$(echo $completion_exists | grep function)" ]; then
      completion $prev $cur
    fi
  fi
}

complete -F _utils utils
